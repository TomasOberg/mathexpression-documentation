/**
 * @file   MathExpressionText.cpp
 * @date   2016-05-09
 * @author Dragan Doga (drgo1200@student.miun.se) Tomas Öberg (tobe1403@student.miun.se)
 * @brief  Testing av MathExpression med Catch
 * @version 1.0
 */

#include <catch.hpp>
#include "MathExpression.h"

TEST_CASE("MathExpression TestCase") {
    MathExpression mathExpression("Hello World!");

    SECTION("Test isValid - Hello World!") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("(2+3");
    SECTION("Test isValid - Unbalanced parantheses (left)") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("2+3)");
    SECTION("Test isValid - Unbalanced parantheses (right)") {
        REQUIRE( !mathExpression.isValid()  );
    }

    mathExpression = MathExpression("35+");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("35 +");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("35 +    ");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }


    mathExpression = MathExpression("35-");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }
    mathExpression = MathExpression("35/");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }
    mathExpression = MathExpression("35*");
    SECTION("Test isValid - Test 35+") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("2--8");
    SECTION("Test isValid - Test 2--8") {
        REQUIRE( mathExpression.isValid() );
    }

    mathExpression = MathExpression("2//8");
    SECTION("Test isValid - Test 2//8") {
        REQUIRE( !mathExpression.isValid() );
    }


    mathExpression = MathExpression("2**8");
    SECTION("Test isValid - Test 2**8") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("2++8");
    SECTION("Test isValid - Test 2++8") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("2+++8");
    SECTION("Test isValid - Test 2+++8") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("2---8");
    SECTION("Test isValid - Test 2---8") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("2---+++8");
    SECTION("Test isValid - Test 2---+++8") {
        REQUIRE( !mathExpression.isValid() );
    }

    mathExpression = MathExpression("1 2 3 ");
    SECTION("Test isValid - 1 2 3 ") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("3 3 + 3");
    SECTION("Test isValid - 3 3 + 3") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("2");
    SECTION("Test isValid: 2") {
        REQUIRE( mathExpression.isValid() );
    }

    mathExpression = MathExpression("(35)");
    SECTION("Test isValid: (35)") {
        REQUIRE( mathExpression.isValid() );
    }
    mathExpression = MathExpression("(1*2)(3+56)");
    SECTION("Test isValid - (1*2)(3+56)") {
        REQUIRE(!mathExpression.isValid() );
    }

    mathExpression = MathExpression("(32+5)-(2+1)");
    SECTION("Test isValid: (32+5)-(2+1)") {
    REQUIRE( mathExpression.isValid() );
    }
    mathExpression = MathExpression("5*(21+1)-1");
    SECTION("Test isValid: 5*(21+1)-1") {
    REQUIRE( mathExpression.isValid() );}

    mathExpression = MathExpression("(2)+(3)");
    SECTION("Test isValid: 5*(21+1)-1") {
    REQUIRE( mathExpression.isValid() );}


    /*
     *POSTFIX
     *  V
     */


    mathExpression = MathExpression("5 + (7*3)");
    SECTION("Test postfixNotation: 5 + (2*3)") {
    REQUIRE(mathExpression.postfixNotation() == "5 7 3 * +");}

    mathExpression = MathExpression("5*(27+3*7) + 22");
    SECTION("Test postfixNotation: 5*(27+3*7) + 22") {
    REQUIRE(mathExpression.postfixNotation() == "5 27 3 7 * + * 22 +");}

    mathExpression = MathExpression("5*6*4");
    SECTION("Test postfixNotation: 5*6*4") {
    REQUIRE(mathExpression.postfixNotation() == "5 6 * 4 *");}

    mathExpression = MathExpression("6+9-7");
    SECTION("Test postfixNotation: 6+9-7") {
    REQUIRE(mathExpression.postfixNotation() == "6 9 + 7 -");}

    mathExpression = MathExpression("2--5");
    SECTION("Test calculate: 2--5") {
    REQUIRE(mathExpression.postfixNotation() == "2 5 +");}


    /*
     *CALCULATE
     *  V
     */


    mathExpression = MathExpression("5 + (2*3)");
    SECTION("Test calculate: 5 + (2*3)") {
    REQUIRE(mathExpression.calculate() == 11.0);}

    mathExpression = MathExpression("5*(27+3*7) + 22");
    SECTION("Test calculate: 5*(27+3*7) + 22") {
    REQUIRE(mathExpression.calculate() == 262.0);}

    mathExpression = MathExpression("5*6*4");
    SECTION("Test calculate: 5*6*4") {
    REQUIRE(mathExpression.calculate() == 120.0);}

    mathExpression = MathExpression("6+9-7");
    SECTION("Test calculate: 6+9-7") {
    REQUIRE(mathExpression.calculate() == 8.0);}

    mathExpression = MathExpression("2--5");
    SECTION("Test calculate: 2--5") {
    REQUIRE(mathExpression.calculate() == 7.0);}


    /*
     * operator=
     *  V
     */

    mathExpression = MathExpression("5 + (2*3)");
    MathExpression tmpMath = mathExpression;
    SECTION("Test operator=: 5 + (2*3)") {
    REQUIRE(tmpMath.infixNotation() == "5 + (2*3)");}

}

