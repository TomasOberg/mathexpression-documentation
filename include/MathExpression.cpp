/**
 * @file   MathExpression.cpp
 * @date   2016-05-07
 * @author Dragan Doga (drgo1200@student.miun.se) Tomas Öberg (tobe1403@student.miun.se)
 * @brief  Klass för lagring och beräkning av enkla matematiska uttryck
 * @version 1.0
 */

#include <vector>
#include "MathExpression.h"
#include <iostream>
#include <sstream>

using namespace std;

/**
 * Hämtar en prioritet för en matteoperator
 * @param tmp en char med en operator
 * @return prioritet för ev operatorn i heltalsform (int)
 */
int getPriority(char tmp)
{
    if(tmp == '*' || tmp == '/')
    {
        return 2;
    }
    else if (tmp == '+' || tmp == '-')
    {
        return 1;
    }

    else
    {
        return 0;
    }
}

/**
 * Kollar om tecknet är en operator
 * @param oper en char att analyseras
 * @return returnerar true/false beroende om char är en matematisk operator eller inte
 */
bool isOperator(char oper)
{
    switch (oper) {
    case '*':
    case '+':
    case '-':
    case '/':
        return true;
    default:
        return false;
    }
}

/**
 * Konstruktor, tar ett uttryck som argument och analyserar det
 * @param expression en sträng som innehåller ett matematiskt uttryck
 */
MathExpression::MathExpression(const std::string &expression)
{
    mathexpression = expression;
    errMessage = "";
    valid = validate();
}

/**
 * @return uttrycket i infix-format
 */
string MathExpression::infixNotation() const
{
    return mathexpression;
}


/**
 * Analyserar uttrycket och konverterar det från infix- till postfixformat
 * @return uttrycket i postfix-format
 */

string MathExpression::postfixNotation() const
{
    stack<char> operands;
    string x = mathexpression;
    string output = "";


    // Parsa (läs) input vänster>
    // höger och överför till output.

    for(int i =0; i<x.length(); i++) {

        if(x[i] == ' ')
        {

        }
        else if(isdigit(x[i])) // en operand (tal) överförs direkt till output
        {


            if(output.length() > 0)
            {
            if((output[output.length() -1 ] != ' ') && !isdigit(output[output.length() -1 ]))
            {
                output+= ' ';
            }
            }

            output+= x[i];

        }
        else if(isOperator(x[i])) //enoperator (+,*, osv) måste sparas på en stack tills dess operander har processats.
            {

                /*Om en operator som ska pushas på stacken har samma eller lägre prioritet än operatorn som ligger
                överst på stacken så poppas den operatorn av stacken och läggs till output innan den nya pushas.*/
            output += ' ';
               while(!operands.empty() && ( getPriority(x[i]) <= getPriority(operands.top()) ))
               {
                   output += operands.top();
                   operands.pop();
               }
               if( (x[i] == '-') && (x.length()>i+1) && (x[i+1] == '-') )
               {
                   operands.push('+');i++;
               }
               else
               {
                   operands.push(x[i]);}
            }

        else if(x[i] == '(') //En vänsterparentes pushas direkt på stacken, oavsett vad som råkar ligga överst på stacken.
        {
            operands.push('(');
        }


        /*När motsvarande höger parentes påträffas så poppas allt som ligger på stacken till output framtill
         * vänsterparentesen. När den påträffas så poppas den av stacken men överförs inte till output.
         */
        else if(x[i] == ')')
        {
            bool poppin = true;
            while(poppin && !operands.empty())
            {
                if(operands.top() == '(')
                {
                    operands.pop();
                    poppin=false;
                }
                else
                {

                    if( output[output.length() -1 ] != ' ')
                    {
                        output+= ' ';
                    }

                    output += operands.top();
                    operands.pop();
                }
            }

        }

    }

    while(!operands.empty())
    {
        if( output[output.length() -1 ] != ' ')
        {
            output+= ' ';
        }

        output+=operands.top();
        operands.pop();
    }

    return output;
}


/**
 * Kalkylerar ett matematiskt uttryck
 * @return resultatet av det uträknade uttrycket
 */
double MathExpression::calculate() const
{
    string x = postfixNotation(); //Lagrar postfix i x

    stack<double> operands; //stack för tal

    for (int i=0; i<x.length(); i++) //itererar igenom postfix
    {
        int next = i+1; // variabel som alltid är +1 position

        if(x[i] == ' ') //om x är tom gör inget
        {

        }
        else
        {
            if(isdigit(x[i])) //om en siffta
            {
                /*double tmpNum = atof(&x);
                operands.push(tmpNum);*/
                string tmpVal = "";
                tmpVal += x[i];


                //om det finns en siffta direkt efter siffran på plats i körs denna while loopen
                //alltså kommer tal större än bara 1 siffra kunna räknas på
                while((next < x.length()) && (x[next] != ' ') && !isOperator(x[next]))
                {
                    tmpVal += x[next];
                    i=next;
                    next++;
                }

                operands.push(stod(tmpVal)); //lagra talet
            }
            else if(isOperator(x[i])) //om x är en operator
            {
                if(operands.size() >= 2) //
                {
                    //tar upp 2 tal från stacken
                    double a = operands.top();
                    operands.pop();
                    double b = operands.top();
                    operands.pop();

                    switch(x[i]) //räknar dem med rätt operator och trycker sedan tillbaka svaret
                    {
                    case '*': operands.push(b*a);
                        break;
                    case '/': operands.push(b/a);
                        break;
                    case '+': operands.push(b+a);
                        break;
                    case '-': operands.push(b-a);
                        break;
                    }

                }
                else
                {
                    cout<<"Something is wrong.. not enought operands"<<endl;
                }
            }
        }
    }

    return operands.top(); //om allt har gått rätt ska svaret finnas på toppen

}

/**
 * Kontrollerar om det matematiska uttrycket är korrekt gjort
 * @return true/false beroende på om uttrycket är korrekt
 */
bool MathExpression::isValid() const {
    return valid;
}

/**
 * Validerar uttrycket utefter ett visst antal regler.
 * @return true/false beroende på om uttrycket är korrekt
 */
bool MathExpression::validate()
{
    int foundLeftParanthesis  =  0;
    int foundRightParanthesis  =  0;
    enum  LAST {blank, digit, op } last;
    std::vector<LAST> category;
    std::vector<int> categoryPosition;

    for(int x=0; x < mathexpression.length(); x++) {

        //---------------------------------------------------------------------
        // Tillåt ej alfanumeriska bokstäver
        //---------------------------------------------------------------------
        if( isalpha(mathexpression[x]))  {
            errMessage = "Bara siffror och +-/*() är tillåtna tecken!";
            return false;
        }

        //---------------------------------------------------------------------
        // Räkna paranteser
        // -----------------
        //
        // Öka på vänsterparantesantalet med ett för varje (
        //---------------------------------------------------------------------
        if(mathexpression[x]=='(') {
            foundLeftParanthesis++;
        }
        //---------------------------------------------------------------------
        // Öka på högerparantesantalet med ett för varje )
        //---------------------------------------------------------------------
        if(mathexpression[x]==')') {
            foundRightParanthesis++;

            //-------------------------------------------------------------------------
            // kontrollera att (1*2)(3+56) inte är giltigt.
            // Dvs: krävs operator mellan parantesparen
            //-------------------------------------------------------------------------
            if(x+1 < mathexpression.length() && mathexpression[x+1]=='(' ) {
                errMessage="krävs operator mellan parantesparen";
                return false;
            }

        }

        if(mathexpression[x] == ' ') {
            categoryPosition.push_back(mathexpression[x]);
            category.push_back(blank);  // Blanksteg
        }
        if(isdigit(mathexpression[x]))  {
            category.push_back(digit);
            categoryPosition.push_back(mathexpression[x]);
        }  // Siffra
        if(isOperator(mathexpression[x]))    {
            category.push_back(op);
            categoryPosition.push_back(mathexpression[x]);
        }     // Operator


    }

    //-------------------------------------------------------------------------
    // Tillåt ej att det sista tecknet (förutom blanksteg) är en operator
    // Kontrollera först att sista tecknet som inte är blanksteg
    // (iterera bakåt till en operator är funnen)
    //-------------------------------------------------------------------------
    int lastToken = mathexpression.length()-1;
    while( mathexpression[lastToken] == ' ') lastToken--;
    if(isOperator(mathexpression[lastToken])) {
        errMessage="Sista tecknet kan inte vara en operand!";
        return false;
    }

    //-------------------------------------------------------------------------
    // Om antal vänsterparanteser inte är samma som antal högerparanteser är det fel:
    //-------------------------------------------------------------------------
    if(foundLeftParanthesis != foundRightParanthesis) {
        errMessage="Obalanserade paranteser";
        return false;
    }

    //-------------------------------------------------------------------------
    // Kontrollera att blanksteg inte får finnas utan operator mellan
    // tex: 2 1 3
    //-------------------------------------------------------------------------
    for(int x=0; x < category.size(); x++) {
        if(x+2 < category.size()) {
            // Om vi får två tal i följd med blanksteg emellan
            if (category[x] == digit && category[x + 1] == blank &&  category[x + 2] == digit) {
                errMessage="Saknas operator mellan talen!";
                return false;
            }
            if (category[x] == op && category[x + 1] == op) {           // Två operatorer i följd,
                if(categoryPosition[x] == categoryPosition  [x + 1]) {  // kolla om de är samma
                    if(categoryPosition[x]=='-') {
                        if(categoryPosition[x + 2] && categoryPosition[x] == categoryPosition[x + 2]) {
                            errMessage="Tre minustecken efter varandra är fel";
                            return false;
                        }
                        return true;
                    }
                    // I övrigt får det inte komma två lika operatorer i följd
                    errMessage="Två operatorer i följd  efter varandra är fel";
                    return false;
                }
            }
        }
    }
    return true;
}


/**
 * @return en sträng med ett eventuellt felmeddelande om uttrycket fel-
 * validerats, annars tom
 */
std::string MathExpression::errorMessage() const
{
    return errMessage;
}

/**
 * Överlagrar "=" för att kunna skapa ny uttryck via en gammal referens/pekare
 * som exempelvis uttrycket mathExpression = MathExpression("3+2");
 *
 * @param expression en sträng med ett matematiskt uttryck
 * @return en adress till denna instans
 */
MathExpression &MathExpression::operator=(const std::string &expression)
{
    this->mathexpression = expression;
    return *this;
}
