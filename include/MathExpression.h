/**
 * @file   MathExpression.h
 * @date   2016-05-07
 * @author Dragan Doga (drgo1200@student.miun.se) Tomas Öberg (tobe1403@student.miun.se)
 * @brief  Klass för lagring och beräkning av enkla matematiska uttryck
 * @version 1.0
 */

#ifndef MATHEXPRESSION_H
#define MATHEXPRESSION_H

#include <string>
#include <stack>
#include <iostream>

/**
 * Klass för lagring och beräkning av enkla matematiska uttryck.
 */
class MathExpression
{
public:
    /**
     * Skapar ett MathExpression från en sträng.
     */
    MathExpression(const std::string &expression);

    /**
     * Returnerar en sträng som är identisk med den som gavs till konstruktorn.
     */
    std::string infixNotation() const;

    /**
     * Returnerar en uttrycket på postfix from
     */
    std::string postfixNotation() const;

    /**
     * Beräknar postfix uttrycket och returnerar svaret.
     */
    double calculate() const;

    /**
     * Returnerar falskt om uttrycket är ogiltigt.
     */
    bool isValid() const;

    /**
     * Returnerar ett felmeddelande om uttrycket är falskt.
     */
    std::string errorMessage() const;

    /**
     * Ersätter befintligt uttryck med expression (nollställer objektet).
     */
    MathExpression& operator=(const std::string& expression);

private:
    std::string mathexpression;     //!< Innehåller det matematiska uttrycket
    std::string errMessage;         //!< Felmeddelande om validering misslyckas
    bool validate();                //!< validerar uttrycket
    bool valid;                     //!< lagrar resultatet av valideringen
};

#endif // MATHEXPRESSION_H

