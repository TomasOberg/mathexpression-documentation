/**
 * @file   main.cpp
 * @date   2016-05-07
 * @author Dragan Doga (drgo1200@student.miun.se) Tomas Öberg (tobe1403@student.miun.se)
 * @brief  Program för att interaktivt testa MathExpresionklassen
 * @version 1.0
 */

#include <memstat.hpp>
#include <iostream>
#include <sstream>
#include <stack>
#include <cctype>

using namespace std;

#include "MathExpression.h"

/**
 * Drar igång en interaktiv loop med en instans av MathExpression och där användaren kan mata in uttryck
 * och få svar/felmeddelanden på uträkningen.
 * @param tmp a char with a math operator
 */
int main()
{
    cout << "MathExpression REPL (Read-Eval-Print-Loop)." << endl << "Type quit to exit." << endl;

    string input;

    cout << "Enter a math expression : ";
    getline(cin, input);

    while(input != "quit")
    {
        MathExpression me(input);

        if(me.isValid())
        {
            cout << "'" << me.infixNotation() << "' ==> '"
                 << me.postfixNotation() << "' = '"
                 << me.calculate() << "'" << endl;
        }
        else
        {
            cout << me.errorMessage() << endl;
        }

        cout << "Enter a math expression : ";
        getline(cin, input);
    }

    return 0;
}

